terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "jjtech-statefile-2023"
    key            = "jjtech/terraform.tfstate"
    region         = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "jjtech-dynamodb"
  }
 }
  