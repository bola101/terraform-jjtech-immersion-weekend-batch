

#########################################
###Module for ec2

module "ec2" {
  source               = "../module/ec2"
  ami_id_module        = "ami-xxxx"
  create-instance = true
  instance_type_module = "t3.small"
  
  security_group_id    = module.security-group.sec-id # module.security-group.sec-id
}

module "vpc" {
  source = "../module/vpc"
}

module "security-group" {
  source = "../module/security-group"

}


# resource "aws_vpc" "name" {
#   cidr_block = "10.10.0.0/16"

# }


# module "web_server_sg" {
#   source = "terraform-aws-modules/security-group/aws//modules/http-80"
#   version = "4.10.0"

#   name        = var.sg-name
#   description = "Security group for web-server with HTTP ports open within VPC"
#   vpc_id      = aws_vpc.name.id

#   ingress_cidr_blocks = ["10.10.0.0/16"]
# }


# output "security_group_arn" {
#   value = module.web_server_sg.security_group_arn
# }








































