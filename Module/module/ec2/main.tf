resource "aws_instance" "ec2_module" {
    count = var.create-instance == true ? 1 : 0
    ami = var.ami_id_module
    instance_type = var.instance_type_module
    vpc_security_group_ids = [var.security_group_id ]
    tags = {
      Name ="${terraform.workspace}-instance"
    }
}




